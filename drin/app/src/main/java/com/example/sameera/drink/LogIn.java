package com.example.sameera.drink;

import android.content.Intent;
import android.graphics.Color;
import android.icu.text.UnicodeSetSpanner;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.text.TextUtilsCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class LogIn extends AppCompatActivity {

    private Button b1, b2;
    private EditText email, password;
    private FirebaseAuth mAuth;


    private TextView txt1, txt2;
    //private int counter = 3;


    DatabaseReference databaseUsers;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log_in);
        mAuth = FirebaseAuth.getInstance();
        databaseUsers = FirebaseDatabase.getInstance().getReference("user");
        b1 = (Button) findViewById(R.id.button1);
        email = (EditText) findViewById(R.id.email);
        password = (EditText) findViewById(R.id.password);

        b2 = (Button) findViewById(R.id.button2);
        txt1 = (TextView) findViewById(R.id.textView);
        txt2 = (TextView) findViewById(R.id.textView2);


        b1.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onClick(View v) {
                String em = email.getText().toString().trim();
                String pw = password.getText().toString().trim();

                //addUser();
                mAuth.createUserWithEmailAndPassword(em , pw).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if(task.isSuccessful())
                        {
                            Toast.makeText(getApplicationContext() , "User Registered",Toast.LENGTH_LONG).show();
                            startActivity(new Intent(LogIn.this, maps.class));

                        }
                        else{

                            if(task.getException() instanceof FirebaseAuthUserCollisionException)
                            {
                                Toast.makeText(getApplicationContext(),"Welcome Back", Toast.LENGTH_SHORT).show();
                                startActivity(new Intent(LogIn.this, maps.class));
                            }
                        }
                    }
                })
                ;

            }

        });
        b2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(LogIn.this, maps.class));
            }
        });

    }

}
   /* public void addUser()
    {

        String em = email.getText().toString().trim();
        String pw = password.getText().toString().trim();

        if(!TextUtils.isEmpty(em) || !TextUtils.isEmpty(pw))
        {


           String id =   databaseUsers.push().getKey();
            users user = new users(id, em, pw);
            databaseUsers.child(id).setValue(user);
            Toast.makeText(this, "User Added" ,Toast.LENGTH_LONG).show();

        }else
        {
            Toast.makeText(this , "Email and password is are not to be empty" , Toast.LENGTH_LONG).show();
        }
    }*/





