package com.example.sameera.drink;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.provider.SyncStateContract;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.security.Provider;

public class maps extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    //private LocationManager lm;
    private GPSTracker gpsTracker ;
    private Location mlocation;
    double latitude , longitude;
    Button add;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        gpsTracker = new GPSTracker(getApplicationContext());
        mlocation = gpsTracker.getLocation();

        latitude = mlocation.getLatitude();
        longitude = mlocation.getLongitude();
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        add = (Button) findViewById(R.id.Add);
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(maps.this, add.class));
                }
        });
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in Sydney and move the camera
        LatLng sydney = new LatLng(latitude ,longitude);
        mMap.addMarker(new MarkerOptions().position(sydney).title("I mam here"));
        //mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(sydney,
                18.0f));

    }/*
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mMap.setMyLocationEnabled(true);
// current location

        // //getting location manager object from systemService
        lm = (LocationManager) getSystemService(LOCATION_SERVICE);

        Criteria criteria = new Criteria();

        String provider = lm.getBestProvider(criteria, true);
        Location location = lm.getLastKnownLocation(provider);
        //drawMarker(location);






    }

    /*private void drawMarker(Location currentLocation) {
        try
        {
            // TODO Auto-generated method stub
            mMap.clear();
            if (currentLocation != null) {
                LatLng currentLatlng = new LatLng(currentLocation.getLatitude(),
                        currentLocation.getLongitude());
                mMap.addMarker(new MarkerOptions()
                        .position(currentLatlng)
                        .snippet(
                                "Lat:" + currentLocation.getLatitude() + "Lng:"
                                        + currentLocation.getLongitude())
                        .title("ME"));

                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(
                        new LatLng(currentLocation.getLatitude(), currentLocation
                                .getLongitude()), 10.0f));


                Location location = mMap.getMyLocation();
                LatLng myLocation = null;
                if (location != null) {
                     myLocation = new LatLng(location.getLatitude(),
                            location.getLongitude());
                }
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(myLocation,
                        21.0f));

            } else {

            }
        } catch (Exception e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();

        }

    }*/

}
