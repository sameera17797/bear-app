package com.example.sameera.drink;

import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.File;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

     Button login ;
    private StorageReference mStorageRef;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);



          findViewById(R.id.Drink).setOnClickListener(this);


    }
    @Override
    public void onClick(View view){
        switch(view.getId()){
            case R.id.Drink:

                startActivity(new Intent(MainActivity.this, LogIn.class));
                break;



        }


    }
}
